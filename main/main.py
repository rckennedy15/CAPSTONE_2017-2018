# -*- coding: utf-8 -*-
"""
@author: Ryan Kennedy
"""
# TODO add support for multi-tracked music (beside just two-tracked piano music

import glob
import os
import re
import subprocess
from os.path import expanduser
from shutil import copyfile

import music21

home_dir = expanduser("~")


def get_file_name_no_extension(file: str) -> str:
	"""
	Remove the file extension.

	:param file: -- string of filename
	:return: -- string of filename without extension
	"""

	return re.search(r'^(.+)\..+', file).groups()[0]


def convert_midi_to_abc() -> None:
	"""Convert the midi files in "midi_transposed" to abc files in "abc_origin" """

	os.chdir('midi_origin')
	transpose()
	os.chdir('..')
	os.chdir('midi_transposed')
	midi_file_list = glob.glob('*.mid')
	os.chdir('..')

	for midi_file in midi_file_list:
		with open('abc_origin/' + get_file_name_no_extension(midi_file) + '.abc', 'wb') as out:
			subprocess.call(['./midi2abc', 'midi_transposed/' + midi_file, '-ga', '-gk', ], stdout=out)


def transpose() -> None:
	"""
	converts all midi files in the current folder
	original  author: https://gist.github.com/aldous-rey/68c6c43450517aa47474

	convert everything into the key of C major or A minor
	"""

	# major conversions
	global half_steps
	majors = dict([
		('A-', 4), ('G# ', 4), ('A', 3), ('A#', 2),
		('B-', 2), ('B', 1), ('C', 0), ('C#', -1),
		('D-', -1), ('D', -2), ('D#', -3), ('E-', -3),
		('E', -4), ('F', -5), ('F#', 6), ('G-', 6), ('G', 5)
	])

	# minor conversions
	minors = dict([
		('G#', 1), ('A-', 1), ('A', 0), ('A#', -1),
		('B-', -1), ('B', -2), ('C', -3), ('C#', -4),
		('D-', -4), ('D', -5), ('D#', 6), ('E-', 6),
		('E', 5), ('F', 4), ('F#', 3), ('G-', 3), ('G', 2)
	])

	file_list = glob.glob('*.mid')

	for file in file_list:
		score = music21.converter.parse(file)
		key = score.analyze('key')
		#    print key.tonic.name, key.mode
		if key.mode == "major":
			half_steps = majors[key.tonic.name]

		elif key.mode == "minor":
			half_steps = minors[key.tonic.name]

		new_score = score.transpose(half_steps)
		key = new_score.analyze('key')
		print(key.tonic.name, key.mode)
		new_file_name = "C_" + file
		os.chdir('..')
		os.chdir('midi_transposed')
		new_score.write('midi', new_file_name)
		os.chdir('..')
		os.chdir('midi_origin')


def combine_abc() -> list:
	"""
	Combine the abc files in "abc_origin" and write to "combined_abc.abc" in
	"preprocessed_data"

	:return: -- list of each line read from each abc file
	"""

	os.chdir('abc_origin')
	abc_files = glob.glob('*.abc')
	os.chdir('..')

	with open('preprocessed_data/combined_abc.abc', 'wb') as combined_abc:
		for abc_file in abc_files:
			with open('abc_origin/' + abc_file, 'rb') as abc:
				combined_abc.write(abc.read())

	with open('preprocessed_data/combined_abc.abc', 'r') as combined_abc:
		return combined_abc.readlines()


def strip_combined_abc(combined_abc_lines: []) -> None:
	"""
	Remove unneeded headers and comments from combined_abc

	:param combined_abc_lines:
	:return: -- None, but writes to "combined_stripped_abc.abc" in
	"preprocessed_data"
	"""

	with open('preprocessed_data/combined_stripped_abc.abc', 'w') as stripped_abc:
		unneeded = False
		for line in combined_abc_lines:
			if line.startswith('V:1'):
				unneeded = False
			if line.startswith('V:3'):
				unneeded = True
			elif (
					not line.startswith('%')
					and not line.startswith('X')
					and not line.startswith('T')
					and not line.startswith('M')
					and not line.startswith('L')
					and not line.startswith('Q')
					and not line.startswith('K')
					and not line.startswith('Error')
					and not unneeded
			):
				stripped_abc.write(line)


def pre_process() -> None:
	"""
	Call the main pre-processing script included in torch-rnn

	Uses python 2.7
	:return: -- None, but writes "torch-rnn/data/input.h5" and .json
	"""

	subprocess.call([
		'python', 'torch-rnn/scripts/preprocess.py',
		'--input_txt', 'preprocessed_data/combined_stripped_abc.abc',
		'--output_h5', 'preprocessed_data/input.h5',
		'--output_json', 'preprocessed_data/input.json'
	])

	copyfile('preprocessed_data/input.h5', 'torch-rnn/data/input.h5')
	copyfile('preprocessed_data/input.json', 'torch-rnn/data/input.json')


def train(model_type='lstm', rnn_size=128, num_layers=2) -> None:
	os.chdir('torch-rnn')

	subprocess.call([
		home_dir + '/torch/install/bin/th', 'train.lua',
		'-input_h5', 'data/input.h5',
		'-input_json', 'data/input.json',
		'-gpu', '-1',
		'-model_type', str(model_type),
		'-rnn_size', str(rnn_size),
		'-num_layers', str(num_layers)
	])

	os.chdir('..')


def sample(checkpoint='cv/checkpoint_1000.t7', length=2000) -> None:
	with open('abc_final/abc_final.abc', 'wb') as out:
		os.chdir('torch-rnn')
		subprocess.call([
			home_dir + '/torch/install/bin/th', 'sample.lua',
			'-checkpoint', str(checkpoint),
			'-length', str(length),
			'-gpu', '-1'
		], stdout=out)

	os.chdir('..')


def post_process() -> None:
	unstrip()

	with open('abc_final/processed_abc_final.abc', 'wb') as out:
		subprocess.call(['./abc2abc', 'abc_final/unstripped_abc.abc', '-s', '-r'], stdout=out)


# This error needs to be fixed:
# %Error : Missing /
# %Error : Expecting fraction in form A/B


def unstrip() -> None:
	with open('abc_final/unstripped_abc.abc', 'w') as out:
		out.write('X:1\n')
		out.write('T:Made with an awesome Neural Network\n')
		out.write('M:None\n')
		out.write('Q:1/4=120\n')
		out.write('K:C\n')
		out.write('L:1/8\n')
		out.write('%%MIDI channel 1\n')

		with open('abc_final/abc_final.abc', 'r') as f:
			out.writelines(f.readlines())


def convert_abc_to_midi() -> None:
	"""UNFINISHED -- Convert abc file to a midi file"""
	with open('abc_final/conversion_log.txt', 'wb') as out:
		subprocess.call(['./abc2midi', 'abc_final/unstripped_abc.abc'], stdout=out)


if __name__ == '__main__':
	running = True
	os.chdir('..')

	choice = int(input(
			'1.) Start from scratch \n2.) Generate new checkpoint from data \n3.) Sample from previously '
			'generated checkpoints\n'
	))
	if choice == 1:
		convert_midi_to_abc()
		lines = combine_abc()
		strip_combined_abc(lines)
		pre_process()

	if choice == 1 or choice == 2:
		print('\033[36m')
		nn = int(
				input(
						'What type of Neural Network do you want?\n'
						'1.) LSTM - Slower but better\n'
						'2.) RNN - Faster but less accurate\n'
				)
				or 1
		)

		size = int(input('Size of the NN (must be power of 2 e.g 128, 256, 512, default is 128): \n') or 128)
		layers = int(input('Number of layers (default is 2): \n') or 2)

		if nn == 1:
			train('lstm', size, layers)
		elif nn == 2:
			train('rnn', size, layers)
		else:
			train('lstm', size, layers)

		print('\033[32m', 'Training Complete!')

	os.chdir('torch-rnn/cv/')
	checkpoint_list = glob.glob('*.t7')
	os.chdir('..')
	os.chdir('..')

	print('Select checkpoint to use: ')
	num = 1
	for chk in checkpoint_list:
		print(num, '.) ', chk)
		num += 1

	chkpt = int(input())
	num_char = int(input('Number of characters to sample? (default is 2000): ') or 2000)

	sample('cv/' + checkpoint_list[chkpt - 1], num_char)

	unstrip()
	convert_abc_to_midi()
	print('Midi file successfully processed! \nLocation: abc_final/unstripped_abc[n].mid')
