# MuWriNN - Music Writing Neural Network
## HOWTO:
* [link to torch-rnn](https://github.com/jcjohnson/torch-rnn) -- follow setup instructions here, but no need to 
download the actual rnn
* [link to abcMIDI](https://github.com/leesavide/abcmidi) -- this is included in project, so no need to download
* [link to transposer used in project](https://gist.github.com/aldous-rey/68c6c43450517aa47474) -- also no need to 
install

Make sure that python 2 and python 3 are installed as python and python3, respectively
Once everything is setup correctly, the program can be run with 'python3 main/main.py'

Midi files in /midi_origin can be replaced to produce different types of music. (must be 2-track piano music)
